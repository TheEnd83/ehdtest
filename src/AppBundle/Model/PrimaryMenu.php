<?php

namespace AppBundle\Model;


class PrimaryMenu
{
    private $menu_items = array();

    function __construct()
    {
        $this->setMenuItem(
            array(
                'weight' => 1,
                'url' => '#',
                'title' => 'Home',
                'current' => true
            )
        );

        $this->setMenuItem(
            array(
                'weight' => 2,
                'url' => '#services',
                'title' => 'Services',
                'current' => false
            )
        );

        $this->setMenuItem(
            array(
                'weight' => 3,
                'url' => '#portfolio',
                'title' => 'Portfolio',
                'current' => false
            )
        );

        $this->setMenuItem(
            array(
                'weight' => 4,
                'url' => '#',
                'title' => 'Contact',
                'current' => false
            )
        );
    }

    private function setMenuItem($item_args) {
        $this->menu_items[$item_args['weight']] = array(
            'url' => $item_args['url'],
            'title' => $item_args['title'],
            'current' => $item_args['current'],
        );
    }

    public function getPrimNav() {
        // sorting items by weight
        ksort($this->menu_items, true);
        return $this->menu_items;
    }
}