<?php

namespace AppBundle\Model;


class OpenWeather
{
    private static $units = 'celsius';
    private static $appID = 'b1b15e88fa797225412429c1c50c122a1';

    public static function getWeatherByCity($city) {
        if($response = file_get_contents(self::buildURL($city))) {
            $response_array = json_decode($response);
            $temperature = $response_array->list[0]->main->temp;
            return $temperature;
        }

        return false;
    }

    private static function buildURL($city) {
        return 'http://samples.openweathermap.org/data/2.5/find?q='.$city.'&units='.self::$units.'&appid='.self::$appID;
    }
}