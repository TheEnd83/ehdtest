<?php

namespace AppBundle\Model;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;


class MozCastTemperature
{
    private $MozCastUrl = 'http://mozcast.com/';
    private $container;
    private $cacheDir;
    private $cache_for_seconds;

    public function __construct(Container $container, $cache_for_seconds)
    {
        $this->container = $container;
        $this->cacheDir = $container->getParameter('kernel.cache_dir').'/MozCastTemperature/';
        $this->cache_for_seconds = $cache_for_seconds;
    }

    public function getMozCastTemperature() {
        if($this->cacheIsExpired()) {
            $live_temp = $this->getMozCastTemperatureFromUrl();
            if($live_temp) {
                $this->setCacheTemperature($live_temp);
                return $this->getTextValueTemperature($live_temp);
            }
            return false;
        }
        return $this->getCachedTemperature();
    }

    private function getMozCastTemperatureFromUrl()
    {
        $DOM = new \DOMDocument();
        libxml_use_internal_errors(true);
        $DOM->strictErrorChecking = false;
        $DOM->recover = true;
        $DOM->preserveWhiteSpace = false;
        if($DOM->loadHTMLFile($this->MozCastUrl)) {
            $MozCastxpath = new \DOMXPath($DOM);
            $query = 'body//div[@class="weather weather-big row hidden-phone"]';
            $results = $MozCastxpath->query($query);
            $textTemperature = $results->item(0)->textContent;
            return $this->getTextTemperature($textTemperature);
        }
        return false;
    }

    private function getTextTemperature($textTemperature) {
        $textTemperatures = str_replace('°', '', $textTemperature);
        return $textTemperatures;
    }

    private function getTextValueTemperature($temperature) {
        $temperature = (int) $temperature;
        $text_temperature = '';
        switch (true) {
            case ($temperature <= 65):
                $text_temperature = 'Low';
                break;
            case ($temperature > 65 && $temperature < 75 ):
                $text_temperature = 'Medium';
                break;
            case ($temperature >= 75 ):
                $text_temperature = 'High';
                break;
        }
        return $text_temperature;
    }

    private function setCacheTemperature($live_temp) {
        $filename = time();
        $filepath = $this->cacheDir.$filename.'.txt';

        //Create your own folder in the cache directory
        $fs = new Filesystem();
        try {
            $fs->mkdir(dirname($filepath));
        } catch (IOException $e) {
            echo "An error occured while creating your directory";
        }

        // clear cache folder
        $this->clearCache();
        // write cache file
        file_put_contents($filepath, $live_temp);

        return true;
    }

    private function getCachedTemperature() {
        $cachepath = $this->cacheDir;
        $last_file = $this->getLastCachedFile();
        return trim(file_get_contents($cachepath.$last_file));
    }

    private function cacheIsExpired() {
        $last_file = $this->getLastCachedFile();
        $file_time_array = explode('.', $last_file);
        $file_time = $file_time_array[0];

        $cached_time_seconds = $file_time;
        $now_time_seconds = time();
        $cached_and_now_diff = $now_time_seconds - $cached_time_seconds;
        if( $cached_and_now_diff > $this->cache_for_seconds ) {
            return true;
        }

        return false;
    }

    private function getLastCachedFile() {
        if(! is_dir($this->cacheDir)) {
            return false;
        }
        $files = scandir($this->cacheDir);
        $last_file = max($files);
        return $last_file;
    }

    public function clearCache() {
        $files = scandir($this->cacheDir);
        foreach ($files as $file) {
            if(is_file($this->cacheDir.$file)) {
                unlink($this->cacheDir.$file);
            }
        }
    }
}