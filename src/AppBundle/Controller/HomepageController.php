<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\ContactMessage;
use AppBundle\Form\ContactForm;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Model\MozCastTemperature;
use AppBundle\Model\OpenWeather;
use AppBundle\Model\PrimaryMenu;

class HomepageController extends Controller
{
    public function homepageAction(Request $request)
    {
        // primary menu
        $prim_nav = new PrimaryMenu();
        $menu_items = $prim_nav->getPrimNav();

        // create form
        $contact_message = new ContactMessage();
        $contact_form = $this->createForm(ContactForm::class, $contact_message);

        $contact_form->handleRequest($request);

        // form submission validation
        if ($contact_form->isSubmitted() && $contact_form->isValid()) {

            // if validation is ok save form submission in the db
            $contact_message = $contact_form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact_message);
            $em->flush();
        }

        // mozcast temperature, passing the App container and the cache time in seconds
        $mozcast = new MozCastTemperature($this->container, 5);
        $mozcast_temperature = $mozcast->getMozCastTemperature();

        // open weather
        $london_temperature = OpenWeather::getWeatherByCity('London');

        return $this->render('AppBundle:Homepage:homepage.html.twig', array(
            'title' => 'EHD test',
            'prim_nav' => $menu_items,
            'contact_form' => $contact_form->createView(),
            'mozcast_temperature' => $mozcast_temperature,
            'london_weather' => $london_temperature,
        ));
    }

}
