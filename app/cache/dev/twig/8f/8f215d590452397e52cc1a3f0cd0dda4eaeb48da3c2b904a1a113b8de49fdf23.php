<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_fc631654104c71f65c265674c707f1ed7065b2fd00a410c9d2b148f5c4f07bb2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9c82b8c45e0391cd10db70c3b567b68b6ea86606aed02073458cef2cb3c13ed5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c82b8c45e0391cd10db70c3b567b68b6ea86606aed02073458cef2cb3c13ed5->enter($__internal_9c82b8c45e0391cd10db70c3b567b68b6ea86606aed02073458cef2cb3c13ed5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_9c82b8c45e0391cd10db70c3b567b68b6ea86606aed02073458cef2cb3c13ed5->leave($__internal_9c82b8c45e0391cd10db70c3b567b68b6ea86606aed02073458cef2cb3c13ed5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
