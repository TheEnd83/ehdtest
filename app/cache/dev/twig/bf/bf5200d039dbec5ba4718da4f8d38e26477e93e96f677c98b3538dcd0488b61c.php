<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_202e0f576d3aa778bd00fb8056eb430aaea3634bf53ae4f70e45e4399eec4412 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3c19c91b2d198e8d283aa248cb186bd0f9526d74c2bf6fb303cf9bec1dbaf0b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c19c91b2d198e8d283aa248cb186bd0f9526d74c2bf6fb303cf9bec1dbaf0b2->enter($__internal_3c19c91b2d198e8d283aa248cb186bd0f9526d74c2bf6fb303cf9bec1dbaf0b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_3c19c91b2d198e8d283aa248cb186bd0f9526d74c2bf6fb303cf9bec1dbaf0b2->leave($__internal_3c19c91b2d198e8d283aa248cb186bd0f9526d74c2bf6fb303cf9bec1dbaf0b2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}
