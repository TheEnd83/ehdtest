<?php

/* SensioDistributionBundle::Configurator/layout.html.twig */
class __TwigTemplate_18866e64ede88b77176edc1312295cef2d265b586410a1287728ab58694c997b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "SensioDistributionBundle::Configurator/layout.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4460f7a1a2f14af4c20ae9731e8682b17391d9994b8afe9e6dda898ee8106d7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4460f7a1a2f14af4c20ae9731e8682b17391d9994b8afe9e6dda898ee8106d7c->enter($__internal_4460f7a1a2f14af4c20ae9731e8682b17391d9994b8afe9e6dda898ee8106d7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SensioDistributionBundle::Configurator/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4460f7a1a2f14af4c20ae9731e8682b17391d9994b8afe9e6dda898ee8106d7c->leave($__internal_4460f7a1a2f14af4c20ae9731e8682b17391d9994b8afe9e6dda898ee8106d7c_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_c558b9bad9f30663441ae863f90c1eaa1b4d22913df44bcac9e9413a4e746d54 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c558b9bad9f30663441ae863f90c1eaa1b4d22913df44bcac9e9413a4e746d54->enter($__internal_c558b9bad9f30663441ae863f90c1eaa1b4d22913df44bcac9e9413a4e746d54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/sensiodistribution/webconfigurator/css/configurator.css"), "html", null, true);
        echo "\" />
";
        
        $__internal_c558b9bad9f30663441ae863f90c1eaa1b4d22913df44bcac9e9413a4e746d54->leave($__internal_c558b9bad9f30663441ae863f90c1eaa1b4d22913df44bcac9e9413a4e746d54_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_aac27c3162b82e21a64bbc002d073179f2c24ca203c230d76d854213282b7c80 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aac27c3162b82e21a64bbc002d073179f2c24ca203c230d76d854213282b7c80->enter($__internal_aac27c3162b82e21a64bbc002d073179f2c24ca203c230d76d854213282b7c80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Web Configurator Bundle";
        
        $__internal_aac27c3162b82e21a64bbc002d073179f2c24ca203c230d76d854213282b7c80->leave($__internal_aac27c3162b82e21a64bbc002d073179f2c24ca203c230d76d854213282b7c80_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_ea3376964ae4eac70ae3aad196e4443296b2f2aa3e5eb177ce1f9a22667a8472 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea3376964ae4eac70ae3aad196e4443296b2f2aa3e5eb177ce1f9a22667a8472->enter($__internal_ea3376964ae4eac70ae3aad196e4443296b2f2aa3e5eb177ce1f9a22667a8472_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "    <div class=\"block\">
        ";
        // line 11
        $this->displayBlock('content', $context, $blocks);
        // line 12
        echo "    </div>
    <div class=\"version\">Symfony Standard Edition v.";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["version"]) ? $context["version"] : $this->getContext($context, "version")), "html", null, true);
        echo "</div>
";
        
        $__internal_ea3376964ae4eac70ae3aad196e4443296b2f2aa3e5eb177ce1f9a22667a8472->leave($__internal_ea3376964ae4eac70ae3aad196e4443296b2f2aa3e5eb177ce1f9a22667a8472_prof);

    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        $__internal_609ffd0526860afc492ec9c6d5c427486ad7f1137d1da7262499f00a0c9e928f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_609ffd0526860afc492ec9c6d5c427486ad7f1137d1da7262499f00a0c9e928f->enter($__internal_609ffd0526860afc492ec9c6d5c427486ad7f1137d1da7262499f00a0c9e928f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_609ffd0526860afc492ec9c6d5c427486ad7f1137d1da7262499f00a0c9e928f->leave($__internal_609ffd0526860afc492ec9c6d5c427486ad7f1137d1da7262499f00a0c9e928f_prof);

    }

    public function getTemplateName()
    {
        return "SensioDistributionBundle::Configurator/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 11,  79 => 13,  76 => 12,  74 => 11,  71 => 10,  65 => 9,  53 => 7,  43 => 4,  37 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"TwigBundle::layout.html.twig\" %}

{% block head %}
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/sensiodistribution/webconfigurator/css/configurator.css') }}\" />
{% endblock %}

{% block title 'Web Configurator Bundle' %}

{% block body %}
    <div class=\"block\">
        {% block content %}{% endblock %}
    </div>
    <div class=\"version\">Symfony Standard Edition v.{{ version }}</div>
{% endblock %}
", "SensioDistributionBundle::Configurator/layout.html.twig", "/var/www/symtest/vendor/sensio/distribution-bundle/Sensio/Bundle/DistributionBundle/Resources/views/Configurator/layout.html.twig");
    }
}
