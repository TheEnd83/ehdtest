<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_46cfd528999d0d6d22a172c2b47c8b993885e51259b9eb76150e4e4c22aae5fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e444ccf04f4569cc0cf34c06c599ed05a185228c1925dec9670fd29f96604a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e444ccf04f4569cc0cf34c06c599ed05a185228c1925dec9670fd29f96604a0->enter($__internal_7e444ccf04f4569cc0cf34c06c599ed05a185228c1925dec9670fd29f96604a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_7e444ccf04f4569cc0cf34c06c599ed05a185228c1925dec9670fd29f96604a0->leave($__internal_7e444ccf04f4569cc0cf34c06c599ed05a185228c1925dec9670fd29f96604a0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
