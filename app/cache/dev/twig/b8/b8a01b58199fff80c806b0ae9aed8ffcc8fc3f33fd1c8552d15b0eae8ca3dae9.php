<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_5a0c56c9e83bc756f9830a9ec71fb5174ac9d86de9da25fa702b67c67809c7e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_081dc404f0f1aa7da117d5ae7b195bf06d023b67647073aca347a4ee1417816c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_081dc404f0f1aa7da117d5ae7b195bf06d023b67647073aca347a4ee1417816c->enter($__internal_081dc404f0f1aa7da117d5ae7b195bf06d023b67647073aca347a4ee1417816c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_081dc404f0f1aa7da117d5ae7b195bf06d023b67647073aca347a4ee1417816c->leave($__internal_081dc404f0f1aa7da117d5ae7b195bf06d023b67647073aca347a4ee1417816c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
