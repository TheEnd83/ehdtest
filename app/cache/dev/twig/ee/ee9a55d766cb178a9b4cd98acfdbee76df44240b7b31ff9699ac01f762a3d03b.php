<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_7dbdaaa250d804cd5940f604c7a7b53b18194f4c4e0440d9b929554c0dac9150 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_688ad47f1726393f732c8558fec97c00ee80633c37236fe40b1aee8f98fcda68 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_688ad47f1726393f732c8558fec97c00ee80633c37236fe40b1aee8f98fcda68->enter($__internal_688ad47f1726393f732c8558fec97c00ee80633c37236fe40b1aee8f98fcda68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_688ad47f1726393f732c8558fec97c00ee80633c37236fe40b1aee8f98fcda68->leave($__internal_688ad47f1726393f732c8558fec97c00ee80633c37236fe40b1aee8f98fcda68_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
", "@Framework/Form/attributes.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/attributes.html.php");
    }
}
