<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_4daebcf0457e1986772292ecf494a8bb3b31c9084fe8de52ca71102214d82cdf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d4bc6aa017aa7be227deaf750283000fe6eeb995ad1de20db3d3cea2fc9b3b79 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d4bc6aa017aa7be227deaf750283000fe6eeb995ad1de20db3d3cea2fc9b3b79->enter($__internal_d4bc6aa017aa7be227deaf750283000fe6eeb995ad1de20db3d3cea2fc9b3b79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_d4bc6aa017aa7be227deaf750283000fe6eeb995ad1de20db3d3cea2fc9b3b79->leave($__internal_d4bc6aa017aa7be227deaf750283000fe6eeb995ad1de20db3d3cea2fc9b3b79_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}
