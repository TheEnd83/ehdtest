<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_78f17dd01238e71b4196f47ad617158eb429e79bcf8871bc131c2a0c06456d11 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04b421bc2e639b565287820ec74b5a4ad33290e2e04aa3edece321c8e40c889f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04b421bc2e639b565287820ec74b5a4ad33290e2e04aa3edece321c8e40c889f->enter($__internal_04b421bc2e639b565287820ec74b5a4ad33290e2e04aa3edece321c8e40c889f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_04b421bc2e639b565287820ec74b5a4ad33290e2e04aa3edece321c8e40c889f->leave($__internal_04b421bc2e639b565287820ec74b5a4ad33290e2e04aa3edece321c8e40c889f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
