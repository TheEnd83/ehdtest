<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_49d4f3941b36a116dc0c09a925f5f73a96b3ae953314c1aa096788d64f17d341 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5f5aecb9bddd7a73059065fb4b3d69d8c127c2e70db307d47aa8daaee4df3aea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f5aecb9bddd7a73059065fb4b3d69d8c127c2e70db307d47aa8daaee4df3aea->enter($__internal_5f5aecb9bddd7a73059065fb4b3d69d8c127c2e70db307d47aa8daaee4df3aea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_5f5aecb9bddd7a73059065fb4b3d69d8c127c2e70db307d47aa8daaee4df3aea->leave($__internal_5f5aecb9bddd7a73059065fb4b3d69d8c127c2e70db307d47aa8daaee4df3aea_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
