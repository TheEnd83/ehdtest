<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_935aceb22b893581a7b443144b89a980f5fdb71aa6f9ec88a2a1df1ede699943 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e0bb45fd0c8933f8d2748f292e0fe808b6b6616aaa085f45be073282b192caaa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0bb45fd0c8933f8d2748f292e0fe808b6b6616aaa085f45be073282b192caaa->enter($__internal_e0bb45fd0c8933f8d2748f292e0fe808b6b6616aaa085f45be073282b192caaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_e0bb45fd0c8933f8d2748f292e0fe808b6b6616aaa085f45be073282b192caaa->leave($__internal_e0bb45fd0c8933f8d2748f292e0fe808b6b6616aaa085f45be073282b192caaa_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
