<?php

/* AppBundle:Homepage:homepage.html.twig */
class __TwigTemplate_cb8d09a30d43115dae2d54fe16df6c9b9ef3905052db1db79a99701af2c8262d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle::base.html.twig", "AppBundle:Homepage:homepage.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_62c5f13a78ca4a3ceff959cd300d91b7cf7888c6208c3337fd2019675005604c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_62c5f13a78ca4a3ceff959cd300d91b7cf7888c6208c3337fd2019675005604c->enter($__internal_62c5f13a78ca4a3ceff959cd300d91b7cf7888c6208c3337fd2019675005604c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:Homepage:homepage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_62c5f13a78ca4a3ceff959cd300d91b7cf7888c6208c3337fd2019675005604c->leave($__internal_62c5f13a78ca4a3ceff959cd300d91b7cf7888c6208c3337fd2019675005604c_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_e175bb5c974dc21bc8a2c083adb6f0a51e65d935a45f5215858ffda429c685c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e175bb5c974dc21bc8a2c083adb6f0a51e65d935a45f5215858ffda429c685c6->enter($__internal_e175bb5c974dc21bc8a2c083adb6f0a51e65d935a45f5215858ffda429c685c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        
        $__internal_e175bb5c974dc21bc8a2c083adb6f0a51e65d935a45f5215858ffda429c685c6->leave($__internal_e175bb5c974dc21bc8a2c083adb6f0a51e65d935a45f5215858ffda429c685c6_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_5c17cd41156b6d0c77fad272e90a9ba21acda17521fa9076925249ba7d74b90f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c17cd41156b6d0c77fad272e90a9ba21acda17521fa9076925249ba7d74b90f->enter($__internal_5c17cd41156b6d0c77fad272e90a9ba21acda17521fa9076925249ba7d74b90f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    ";
        // line 7
        echo "    <div style=\"background-image: url('";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/banner.jpg"), "html", null, true);
        echo "')\" class=\"top-banner\">
        <div class=\"teaser\">
            <p>\"I am looking for unexpected.</p>
            <p> I'm looking for things I've never seen before.\"</p>
        </div>
    </div>
    ";
        // line 14
        echo "
    ";
        // line 16
        echo "    <div class=\"services\">
        <div class=\"wrapper\">
            <div class=\"pure-g\">
                <div class=\"section-title pure-u-1\">
                    <span class=\"underline\">Ser</span>vices
                </div>
                <div class=\"service-items\">
                    <div class=\"service-item pure-u-1 pure-u-md-1-4\">
                        <div class=\"service-item-container\">
                            <div class=\"image\"><img src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/blueimg1.png"), "html", null, true);
        echo "\"></div>
                            <div class=\"title\">";
        // line 26
        echo twig_escape_filter($this->env, (((isset($context["mozcast_temperature"]) ? $context["mozcast_temperature"] : $this->getContext($context, "mozcast_temperature"))) ? ((isset($context["mozcast_temperature"]) ? $context["mozcast_temperature"] : $this->getContext($context, "mozcast_temperature"))) : ("Coffee")), "html", null, true);
        echo "</div>
                            <div class=\"content\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        </div>
                    </div>
                    <div class=\"service-item pure-u-1 pure-u-md-1-4\">
                        <div class=\"service-item-container\">
                            <div class=\"image\"><img src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/blueimg2.png"), "html", null, true);
        echo "\"></div>
                            <div class=\"title\">";
        // line 33
        echo twig_escape_filter($this->env, (((isset($context["london_weather"]) ? $context["london_weather"] : $this->getContext($context, "london_weather"))) ? ((isset($context["london_weather"]) ? $context["london_weather"] : $this->getContext($context, "london_weather"))) : ("Istant")), "html", null, true);
        echo "</div>
                            <div class=\"content\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        </div>
                    </div>
                    <div class=\"service-item pure-u-1 pure-u-md-1-4\">
                        <div class=\"service-item-container\">
                            <div class=\"image\"><img src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/blueimg3.png"), "html", null, true);
        echo "\"></div>
                            <div class=\"title\">Serious</div>
                            <div class=\"content\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        </div>
                    </div>
                    <div class=\"service-item pure-u-1 pure-u-md-1-4\">
                        <div class=\"service-item-container\">
                            <div class=\"image\"><img src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/blueimg4.png"), "html", null, true);
        echo "\"></div>
                            <div class=\"title\">Frame</div>
                            <div class=\"content\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    ";
        // line 56
        echo "
    ";
        // line 58
        echo "    <div class=\"portfolio\">
        <div class=\"wrapper\">
            <div class=\"pure-g\">
                <div class=\"section-title pure-u-1\">
                    <span class=\"underline\">Por</span>tfolio
                </div>
                <div class=\"portfolio-items\">
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/portfolio1.jpg"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/portfolio2.jpg"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/portfolio3.jpg"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/portfolio1.jpg"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/portfolio2.jpg"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/portfolio3.jpg"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    ";
        // line 100
        echo "
    <div class=\"divide\">&nbsp;</div>

    ";
        // line 104
        echo "    <div style=\"background-image: url('";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/form-back.jpg"), "html", null, true);
        echo "')\" class=\"contact-form\">
        <div class=\"wrapper\">
            <div class=\"section-title pure-u-1\">
                <span class=\"underline\">Con</span>tact
            </div>
            <div class=\"form-container\">
                ";
        // line 110
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["contact_form"]) ? $context["contact_form"] : $this->getContext($context, "contact_form")), 'form_start');
        echo "
                    ";
        // line 111
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["contact_form"]) ? $context["contact_form"] : $this->getContext($context, "contact_form")), 'errors');
        echo "
                    <div class=\"form-row\">
                        <div class=\"pure-g\">
                            <div class=\"pure-u-1 pure-u-md-1-2 top-input\">
                                <div>
                                    ";
        // line 116
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : $this->getContext($context, "contact_form")), "name", array()), 'label');
        echo "<br />
                                    ";
        // line 117
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : $this->getContext($context, "contact_form")), "name", array()), 'widget');
        echo "
                                </div>
                                <div>
                                    ";
        // line 120
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : $this->getContext($context, "contact_form")), "email", array()), 'label');
        echo "<br />
                                    ";
        // line 121
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : $this->getContext($context, "contact_form")), "email", array()), 'widget');
        echo "
                                </div>
                            </div>
                            <div class=\"pure-u-1 pure-u-md-1-2 form-text\">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class=\"form-row\">
                        <div class=\"pure-g\">
                            <div class=\"pure-u-1\">
                                ";
        // line 133
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : $this->getContext($context, "contact_form")), "message", array()), 'label');
        echo "<br />
                                ";
        // line 134
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : $this->getContext($context, "contact_form")), "message", array()), 'widget', array("attr" => array("rows" => "7")));
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"form-row hidden\">
                        <div class=\"pure-g\">
                            <div class=\"pure-u-1\">
                                ";
        // line 141
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : $this->getContext($context, "contact_form")), "submitted", array()), 'label');
        echo "<br />
                                ";
        // line 142
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : $this->getContext($context, "contact_form")), "submitted", array()), 'widget');
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"form-row\">
                        <div class=\"pure-g\">
                            <div class=\"pure-u-1\">
                                <input type=\"submit\" value=\"Message\">
                            </div>
                        </div>
                    </div>
                ";
        // line 153
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["contact_form"]) ? $context["contact_form"] : $this->getContext($context, "contact_form")), 'form_end');
        echo "
            </div>
        </div>
    </div>
    ";
        // line 158
        echo "

";
        
        $__internal_5c17cd41156b6d0c77fad272e90a9ba21acda17521fa9076925249ba7d74b90f->leave($__internal_5c17cd41156b6d0c77fad272e90a9ba21acda17521fa9076925249ba7d74b90f_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:Homepage:homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  286 => 158,  279 => 153,  265 => 142,  261 => 141,  251 => 134,  247 => 133,  232 => 121,  228 => 120,  222 => 117,  218 => 116,  210 => 111,  206 => 110,  196 => 104,  191 => 100,  181 => 92,  173 => 87,  165 => 82,  157 => 77,  149 => 72,  141 => 67,  130 => 58,  127 => 56,  115 => 46,  105 => 39,  96 => 33,  92 => 32,  83 => 26,  79 => 25,  68 => 16,  65 => 14,  55 => 7,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"AppBundle::base.html.twig\" %}

{% block title %}{{ title }}{% endblock %}

{% block body %}
    {# BANNER #}
    <div style=\"background-image: url('{{ asset('assets/images/banner.jpg') }}')\" class=\"top-banner\">
        <div class=\"teaser\">
            <p>\"I am looking for unexpected.</p>
            <p> I'm looking for things I've never seen before.\"</p>
        </div>
    </div>
    {# END BANNER #}

    {# SERVICES #}
    <div class=\"services\">
        <div class=\"wrapper\">
            <div class=\"pure-g\">
                <div class=\"section-title pure-u-1\">
                    <span class=\"underline\">Ser</span>vices
                </div>
                <div class=\"service-items\">
                    <div class=\"service-item pure-u-1 pure-u-md-1-4\">
                        <div class=\"service-item-container\">
                            <div class=\"image\"><img src=\"{{ asset('assets/images/blueimg1.png') }}\"></div>
                            <div class=\"title\">{{ (mozcast_temperature) ? mozcast_temperature : 'Coffee' }}</div>
                            <div class=\"content\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        </div>
                    </div>
                    <div class=\"service-item pure-u-1 pure-u-md-1-4\">
                        <div class=\"service-item-container\">
                            <div class=\"image\"><img src=\"{{ asset('assets/images/blueimg2.png') }}\"></div>
                            <div class=\"title\">{{ (london_weather) ? london_weather : 'Istant' }}</div>
                            <div class=\"content\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        </div>
                    </div>
                    <div class=\"service-item pure-u-1 pure-u-md-1-4\">
                        <div class=\"service-item-container\">
                            <div class=\"image\"><img src=\"{{ asset('assets/images/blueimg3.png') }}\"></div>
                            <div class=\"title\">Serious</div>
                            <div class=\"content\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        </div>
                    </div>
                    <div class=\"service-item pure-u-1 pure-u-md-1-4\">
                        <div class=\"service-item-container\">
                            <div class=\"image\"><img src=\"{{ asset('assets/images/blueimg4.png') }}\"></div>
                            <div class=\"title\">Frame</div>
                            <div class=\"content\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {# END SERVICES #}

    {# PORTFOLIO #}
    <div class=\"portfolio\">
        <div class=\"wrapper\">
            <div class=\"pure-g\">
                <div class=\"section-title pure-u-1\">
                    <span class=\"underline\">Por</span>tfolio
                </div>
                <div class=\"portfolio-items\">
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"{{ asset('assets/images/portfolio1.jpg') }}\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"{{ asset('assets/images/portfolio2.jpg') }}\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"{{ asset('assets/images/portfolio3.jpg') }}\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"{{ asset('assets/images/portfolio1.jpg') }}\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"{{ asset('assets/images/portfolio2.jpg') }}\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"{{ asset('assets/images/portfolio3.jpg') }}\">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {# END PORTFOLIO #}

    <div class=\"divide\">&nbsp;</div>

    {# CONTACT FORM #}
    <div style=\"background-image: url('{{ asset('assets/images/form-back.jpg') }}')\" class=\"contact-form\">
        <div class=\"wrapper\">
            <div class=\"section-title pure-u-1\">
                <span class=\"underline\">Con</span>tact
            </div>
            <div class=\"form-container\">
                {{ form_start(contact_form) }}
                    {{ form_errors(contact_form) }}
                    <div class=\"form-row\">
                        <div class=\"pure-g\">
                            <div class=\"pure-u-1 pure-u-md-1-2 top-input\">
                                <div>
                                    {{ form_label(contact_form.name) }}<br />
                                    {{ form_widget(contact_form.name) }}
                                </div>
                                <div>
                                    {{ form_label(contact_form.email) }}<br />
                                    {{ form_widget(contact_form.email) }}
                                </div>
                            </div>
                            <div class=\"pure-u-1 pure-u-md-1-2 form-text\">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class=\"form-row\">
                        <div class=\"pure-g\">
                            <div class=\"pure-u-1\">
                                {{ form_label(contact_form.message) }}<br />
                                {{ form_widget(contact_form.message, { 'attr': {'rows': '7'}}) }}
                            </div>
                        </div>
                    </div>
                    <div class=\"form-row hidden\">
                        <div class=\"pure-g\">
                            <div class=\"pure-u-1\">
                                {{ form_label(contact_form.submitted) }}<br />
                                {{ form_widget(contact_form.submitted) }}
                            </div>
                        </div>
                    </div>
                    <div class=\"form-row\">
                        <div class=\"pure-g\">
                            <div class=\"pure-u-1\">
                                <input type=\"submit\" value=\"Message\">
                            </div>
                        </div>
                    </div>
                {{ form_end(contact_form) }}
            </div>
        </div>
    </div>
    {# EMD CONTACT FORM #}


{% endblock %}
", "AppBundle:Homepage:homepage.html.twig", "/var/www/symtest/src/AppBundle/Resources/views/Homepage/homepage.html.twig");
    }
}
