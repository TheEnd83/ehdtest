<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_6a812dcd5fb727d49521393fccf4dd7308d7df0904f68468d731042964f21b97 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_62c9305b888418485843777f01268db9edb26c91c3525a2e70186ecd6b643b80 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_62c9305b888418485843777f01268db9edb26c91c3525a2e70186ecd6b643b80->enter($__internal_62c9305b888418485843777f01268db9edb26c91c3525a2e70186ecd6b643b80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_62c9305b888418485843777f01268db9edb26c91c3525a2e70186ecd6b643b80->leave($__internal_62c9305b888418485843777f01268db9edb26c91c3525a2e70186ecd6b643b80_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_432ce202af380ef6052221214a176d3ba301caed8e6751a580132afe29ee6a76 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_432ce202af380ef6052221214a176d3ba301caed8e6751a580132afe29ee6a76->enter($__internal_432ce202af380ef6052221214a176d3ba301caed8e6751a580132afe29ee6a76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_432ce202af380ef6052221214a176d3ba301caed8e6751a580132afe29ee6a76->leave($__internal_432ce202af380ef6052221214a176d3ba301caed8e6751a580132afe29ee6a76_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_6ee462f5beae1d75ec0a98b82b51f91a20abd550ebe7735a165fe832934599de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6ee462f5beae1d75ec0a98b82b51f91a20abd550ebe7735a165fe832934599de->enter($__internal_6ee462f5beae1d75ec0a98b82b51f91a20abd550ebe7735a165fe832934599de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_6ee462f5beae1d75ec0a98b82b51f91a20abd550ebe7735a165fe832934599de->leave($__internal_6ee462f5beae1d75ec0a98b82b51f91a20abd550ebe7735a165fe832934599de_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
