<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_b073f4d7dc41311e2f759efc80d3f17f3498a9feb56b3042ec8de53baa7d2349 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fbee225f4cc01b849206fdcee1394836c64c681bd9cef7ff34bcb2753662a03a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fbee225f4cc01b849206fdcee1394836c64c681bd9cef7ff34bcb2753662a03a->enter($__internal_fbee225f4cc01b849206fdcee1394836c64c681bd9cef7ff34bcb2753662a03a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_fbee225f4cc01b849206fdcee1394836c64c681bd9cef7ff34bcb2753662a03a->leave($__internal_fbee225f4cc01b849206fdcee1394836c64c681bd9cef7ff34bcb2753662a03a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
