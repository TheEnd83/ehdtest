<?php

/* @App/partials/header.html.twig */
class __TwigTemplate_2e271adb4f4cb0719f7ef0f3eab64c3a33af32ffda87286e3992294955f825aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d169d24c5a6a5101cba4686dabc71549b4fb0d88b0a620f3e519b053f27c81df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d169d24c5a6a5101cba4686dabc71549b4fb0d88b0a620f3e519b053f27c81df->enter($__internal_d169d24c5a6a5101cba4686dabc71549b4fb0d88b0a620f3e519b053f27c81df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/partials/header.html.twig"));

        // line 1
        echo "<div class=\"wrapper\">
    <div class=\"pure-g\">
        <div class=\"logo-container pure-u-6-24\">
            <div class=\"logo\">
                BLU<span>E</span>ASY
            </div>
        </div>
        <div id=\"burger-menu\" class=\"burger-menu pure-u-18-24\">
            <img src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/burger.png"), "html", null, true);
        echo "\">
        </div>
        <div id=\"pri_nav\" class=\"menu-container pure-u-1 pure-u-md-18-24\">
            ";
        // line 12
        if ((isset($context["prim_nav"]) ? $context["prim_nav"] : $this->getContext($context, "prim_nav"))) {
            // line 13
            echo "                <ul class=\"menu\">
                    ";
            // line 14
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["prim_nav"]) ? $context["prim_nav"] : $this->getContext($context, "prim_nav")));
            foreach ($context['_seq'] as $context["_key"] => $context["menu_item"]) {
                // line 15
                echo "                    <li><a class=\"";
                echo (($this->getAttribute($context["menu_item"], "current", array())) ? ("current") : (""));
                echo "\" href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["menu_item"], "url", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["menu_item"], "title", array()), "html", null, true);
                echo "</a></li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu_item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "                </ul>
            ";
        }
        // line 19
        echo "        </div>
    </div>
</div>";
        
        $__internal_d169d24c5a6a5101cba4686dabc71549b4fb0d88b0a620f3e519b053f27c81df->leave($__internal_d169d24c5a6a5101cba4686dabc71549b4fb0d88b0a620f3e519b053f27c81df_prof);

    }

    public function getTemplateName()
    {
        return "@App/partials/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 19,  60 => 17,  47 => 15,  43 => 14,  40 => 13,  38 => 12,  32 => 9,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"wrapper\">
    <div class=\"pure-g\">
        <div class=\"logo-container pure-u-6-24\">
            <div class=\"logo\">
                BLU<span>E</span>ASY
            </div>
        </div>
        <div id=\"burger-menu\" class=\"burger-menu pure-u-18-24\">
            <img src=\"{{ asset('assets/images/burger.png') }}\">
        </div>
        <div id=\"pri_nav\" class=\"menu-container pure-u-1 pure-u-md-18-24\">
            {% if prim_nav %}
                <ul class=\"menu\">
                    {% for menu_item in prim_nav %}
                    <li><a class=\"{{ (menu_item.current) ? 'current' : '' }}\" href=\"{{ menu_item.url }}\">{{ menu_item.title }}</a></li>
                    {% endfor %}
                </ul>
            {% endif %}
        </div>
    </div>
</div>", "@App/partials/header.html.twig", "/var/www/symtest/src/AppBundle/Resources/views/partials/header.html.twig");
    }
}
