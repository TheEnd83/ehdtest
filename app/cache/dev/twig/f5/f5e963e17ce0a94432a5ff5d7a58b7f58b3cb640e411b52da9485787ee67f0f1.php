<?php

/* @App/partials/footer.html.twig */
class __TwigTemplate_a3a5b77990c7a128b74899e94279d07dbdb9ea882b92c179a034d89926ae4ce9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf71fd9e5c37c4cd25df5859e3cb978351e505c5f905a12bfcd2ff3e51f06210 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf71fd9e5c37c4cd25df5859e3cb978351e505c5f905a12bfcd2ff3e51f06210->enter($__internal_cf71fd9e5c37c4cd25df5859e3cb978351e505c5f905a12bfcd2ff3e51f06210_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/partials/footer.html.twig"));

        // line 1
        echo "<div class=\"wrapper\">
    <div class=\"pure-g\">
        <div class=\"pure-u-1 pure-u-md-1-2\">
            <div class=\"footer-menu\">
                <ul>
                    <li><a href=\"#\">Home</a></li>
                    <li><a href=\"#\">Services</a></li>
                    <li><a href=\"#\">Portfolio</a></li>
                    <li><a href=\"#\">Contact</a></li>
                </ul>
            </div>
        </div>
        <div class=\"pure-u-1 pure-u-md-1-2\">
            <div class=\"copyrights\">
                <p>";
        // line 15
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " &copy;</p>
            </div>
        </div>
    </div>
</div>";
        
        $__internal_cf71fd9e5c37c4cd25df5859e3cb978351e505c5f905a12bfcd2ff3e51f06210->leave($__internal_cf71fd9e5c37c4cd25df5859e3cb978351e505c5f905a12bfcd2ff3e51f06210_prof);

    }

    public function getTemplateName()
    {
        return "@App/partials/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 15,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"wrapper\">
    <div class=\"pure-g\">
        <div class=\"pure-u-1 pure-u-md-1-2\">
            <div class=\"footer-menu\">
                <ul>
                    <li><a href=\"#\">Home</a></li>
                    <li><a href=\"#\">Services</a></li>
                    <li><a href=\"#\">Portfolio</a></li>
                    <li><a href=\"#\">Contact</a></li>
                </ul>
            </div>
        </div>
        <div class=\"pure-u-1 pure-u-md-1-2\">
            <div class=\"copyrights\">
                <p>{{ \"now\"|date('Y') }} &copy;</p>
            </div>
        </div>
    </div>
</div>", "@App/partials/footer.html.twig", "/var/www/symtest/src/AppBundle/Resources/views/partials/footer.html.twig");
    }
}
