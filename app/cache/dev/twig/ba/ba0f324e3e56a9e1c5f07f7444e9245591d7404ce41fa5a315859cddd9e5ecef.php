<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_beb68c11c9d099a0e73bf96da727aef331162d61763c53d0bb15994b9235778b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7de12ee335624ed7ccadaa9925348c37022de3420861d774f6bdfbb7e18a4386 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7de12ee335624ed7ccadaa9925348c37022de3420861d774f6bdfbb7e18a4386->enter($__internal_7de12ee335624ed7ccadaa9925348c37022de3420861d774f6bdfbb7e18a4386_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7de12ee335624ed7ccadaa9925348c37022de3420861d774f6bdfbb7e18a4386->leave($__internal_7de12ee335624ed7ccadaa9925348c37022de3420861d774f6bdfbb7e18a4386_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_adb200761a12793aacc5e0f2cab4f933442d57b6f32b31ec77db353c43d97124 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_adb200761a12793aacc5e0f2cab4f933442d57b6f32b31ec77db353c43d97124->enter($__internal_adb200761a12793aacc5e0f2cab4f933442d57b6f32b31ec77db353c43d97124_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_adb200761a12793aacc5e0f2cab4f933442d57b6f32b31ec77db353c43d97124->leave($__internal_adb200761a12793aacc5e0f2cab4f933442d57b6f32b31ec77db353c43d97124_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_21dd3415ceac0de48b2cc44b438266783504de82b83dc87bbf354fb8b38b8ede = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_21dd3415ceac0de48b2cc44b438266783504de82b83dc87bbf354fb8b38b8ede->enter($__internal_21dd3415ceac0de48b2cc44b438266783504de82b83dc87bbf354fb8b38b8ede_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_21dd3415ceac0de48b2cc44b438266783504de82b83dc87bbf354fb8b38b8ede->leave($__internal_21dd3415ceac0de48b2cc44b438266783504de82b83dc87bbf354fb8b38b8ede_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_245e4766c657ea4762ae685b355a8ef67f78f831d0baac68ece51f62633bf222 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_245e4766c657ea4762ae685b355a8ef67f78f831d0baac68ece51f62633bf222->enter($__internal_245e4766c657ea4762ae685b355a8ef67f78f831d0baac68ece51f62633bf222_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_245e4766c657ea4762ae685b355a8ef67f78f831d0baac68ece51f62633bf222->leave($__internal_245e4766c657ea4762ae685b355a8ef67f78f831d0baac68ece51f62633bf222_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
