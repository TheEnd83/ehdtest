<?php

/* AppBundle::base.html.twig */
class __TwigTemplate_7b983da837d19d2a02d5e814eee313e9d8b13cec8ef845391ee1761bc3f0c4d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6188df536a2abf0d9fd6aa47cb7cacf9f9f0b74aed1a788d31ef46b95acce459 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6188df536a2abf0d9fd6aa47cb7cacf9f9f0b74aed1a788d31ef46b95acce459->enter($__internal_6188df536a2abf0d9fd6aa47cb7cacf9f9f0b74aed1a788d31ef46b95acce459_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"stylesheet\" href=\"https://unpkg.com/purecss@0.6.2/build/pure-min.css\" integrity=\"sha384-UQiGfs9ICog+LwheBSRCt1o5cbyKIHbwjWscjemyBMT9YCUMZffs6UqUTd0hObXD\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://unpkg.com/purecss@0.6.2/build/grids-responsive-min.css\">
        <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/styles.css"), "html", null, true);
        echo "\">
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <script src=\"https://code.jquery.com/jquery-3.2.1.min.js\" integrity=\"sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=\" crossorigin=\"anonymous\"></script>
    </head>
    <body>
        <header>
            ";
        // line 15
        echo twig_include($this->env, $context, "@App/partials/header.html.twig");
        echo "
        </header>
        ";
        // line 17
        $this->displayBlock('body', $context, $blocks);
        // line 18
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 19
        echo "        <footer>
            ";
        // line 20
        echo twig_include($this->env, $context, "@App/partials/footer.html.twig");
        echo "
        </footer>
        <script type=\"text/javascript\" src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/scripts.js"), "html", null, true);
        echo "\"></script>
    </body>
</html>
";
        
        $__internal_6188df536a2abf0d9fd6aa47cb7cacf9f9f0b74aed1a788d31ef46b95acce459->leave($__internal_6188df536a2abf0d9fd6aa47cb7cacf9f9f0b74aed1a788d31ef46b95acce459_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_9609bca5bf14f1e69af59b2b0cc17c61f24cfd059cc00a5eddf2d07f9fb3508c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9609bca5bf14f1e69af59b2b0cc17c61f24cfd059cc00a5eddf2d07f9fb3508c->enter($__internal_9609bca5bf14f1e69af59b2b0cc17c61f24cfd059cc00a5eddf2d07f9fb3508c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_9609bca5bf14f1e69af59b2b0cc17c61f24cfd059cc00a5eddf2d07f9fb3508c->leave($__internal_9609bca5bf14f1e69af59b2b0cc17c61f24cfd059cc00a5eddf2d07f9fb3508c_prof);

    }

    // line 17
    public function block_body($context, array $blocks = array())
    {
        $__internal_5deb505856f4d87b8daa90463e9434c2c6b08ca675ec17991da371941321b5d5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5deb505856f4d87b8daa90463e9434c2c6b08ca675ec17991da371941321b5d5->enter($__internal_5deb505856f4d87b8daa90463e9434c2c6b08ca675ec17991da371941321b5d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_5deb505856f4d87b8daa90463e9434c2c6b08ca675ec17991da371941321b5d5->leave($__internal_5deb505856f4d87b8daa90463e9434c2c6b08ca675ec17991da371941321b5d5_prof);

    }

    // line 18
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_3caa84c2e6a06ce8ee174638c099b88e4208a0755ed61e0745fc901db081dc18 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3caa84c2e6a06ce8ee174638c099b88e4208a0755ed61e0745fc901db081dc18->enter($__internal_3caa84c2e6a06ce8ee174638c099b88e4208a0755ed61e0745fc901db081dc18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_3caa84c2e6a06ce8ee174638c099b88e4208a0755ed61e0745fc901db081dc18->leave($__internal_3caa84c2e6a06ce8ee174638c099b88e4208a0755ed61e0745fc901db081dc18_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 18,  91 => 17,  79 => 5,  68 => 22,  63 => 20,  60 => 19,  57 => 18,  55 => 17,  50 => 15,  41 => 9,  37 => 8,  31 => 5,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        <link rel=\"stylesheet\" href=\"https://unpkg.com/purecss@0.6.2/build/pure-min.css\" integrity=\"sha384-UQiGfs9ICog+LwheBSRCt1o5cbyKIHbwjWscjemyBMT9YCUMZffs6UqUTd0hObXD\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://unpkg.com/purecss@0.6.2/build/grids-responsive-min.css\">
        <link rel=\"stylesheet\" href=\"{{ asset('assets/css/styles.css') }}\">
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <script src=\"https://code.jquery.com/jquery-3.2.1.min.js\" integrity=\"sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=\" crossorigin=\"anonymous\"></script>
    </head>
    <body>
        <header>
            {{ include('@App/partials/header.html.twig') }}
        </header>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
        <footer>
            {{ include('@App/partials/footer.html.twig') }}
        </footer>
        <script type=\"text/javascript\" src=\"{{ asset('assets/js/scripts.js') }}\"></script>
    </body>
</html>
", "AppBundle::base.html.twig", "/var/www/symtest/src/AppBundle/Resources/views/base.html.twig");
    }
}
