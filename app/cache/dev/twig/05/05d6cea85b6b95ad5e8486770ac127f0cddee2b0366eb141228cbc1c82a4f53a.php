<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_3d95edf26465ccbb3c54c5b4febf6821a781b274016b3cf21e8525e0d58ef2e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64c58086f28d3f3f788c7e665343df5cd169127d6a78c1175eccdff92e2a3531 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64c58086f28d3f3f788c7e665343df5cd169127d6a78c1175eccdff92e2a3531->enter($__internal_64c58086f28d3f3f788c7e665343df5cd169127d6a78c1175eccdff92e2a3531_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_64c58086f28d3f3f788c7e665343df5cd169127d6a78c1175eccdff92e2a3531->leave($__internal_64c58086f28d3f3f788c7e665343df5cd169127d6a78c1175eccdff92e2a3531_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
