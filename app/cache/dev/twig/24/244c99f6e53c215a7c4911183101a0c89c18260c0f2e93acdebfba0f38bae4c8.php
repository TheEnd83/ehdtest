<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_07d1ad1a7a4d31cebf298f268d05c55d443de7d74093cd197e4aeab09d2d7185 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9f0fb2c5d40503c75eb58589e0eb318f67589c5727c3aabcb8d94afec087977 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c9f0fb2c5d40503c75eb58589e0eb318f67589c5727c3aabcb8d94afec087977->enter($__internal_c9f0fb2c5d40503c75eb58589e0eb318f67589c5727c3aabcb8d94afec087977_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_c9f0fb2c5d40503c75eb58589e0eb318f67589c5727c3aabcb8d94afec087977->leave($__internal_c9f0fb2c5d40503c75eb58589e0eb318f67589c5727c3aabcb8d94afec087977_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}
