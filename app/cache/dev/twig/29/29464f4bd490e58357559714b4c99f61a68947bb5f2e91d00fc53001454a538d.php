<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_e1558431b6cc6c012dccad392dddb8d7ae344378cce0465ff9b35375af31a58d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_91e9cda88434d401b958a64e8cc05e34ba5b93f77f4415a15dfc1730a82c6ca7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91e9cda88434d401b958a64e8cc05e34ba5b93f77f4415a15dfc1730a82c6ca7->enter($__internal_91e9cda88434d401b958a64e8cc05e34ba5b93f77f4415a15dfc1730a82c6ca7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_91e9cda88434d401b958a64e8cc05e34ba5b93f77f4415a15dfc1730a82c6ca7->leave($__internal_91e9cda88434d401b958a64e8cc05e34ba5b93f77f4415a15dfc1730a82c6ca7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
