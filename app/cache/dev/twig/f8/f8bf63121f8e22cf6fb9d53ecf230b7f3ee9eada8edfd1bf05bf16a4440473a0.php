<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_5015cb2b00d01295fdb65913f4eea3ececefa27037fdc25c4eecc95b939d1958 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7fa0fcb5227f9fa281bff6881bfa622c5fd6af2dd6cf7ee039b45ec1c77423e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7fa0fcb5227f9fa281bff6881bfa622c5fd6af2dd6cf7ee039b45ec1c77423e9->enter($__internal_7fa0fcb5227f9fa281bff6881bfa622c5fd6af2dd6cf7ee039b45ec1c77423e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_7fa0fcb5227f9fa281bff6881bfa622c5fd6af2dd6cf7ee039b45ec1c77423e9->leave($__internal_7fa0fcb5227f9fa281bff6881bfa622c5fd6af2dd6cf7ee039b45ec1c77423e9_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_f29e02a5d4f7f3d15d9a4f2f113b5978efeff301c96fcae32253bc395b350702 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f29e02a5d4f7f3d15d9a4f2f113b5978efeff301c96fcae32253bc395b350702->enter($__internal_f29e02a5d4f7f3d15d9a4f2f113b5978efeff301c96fcae32253bc395b350702_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_f29e02a5d4f7f3d15d9a4f2f113b5978efeff301c96fcae32253bc395b350702->leave($__internal_f29e02a5d4f7f3d15d9a4f2f113b5978efeff301c96fcae32253bc395b350702_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
