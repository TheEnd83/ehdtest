<?php

/* ::base.html.twig */
class __TwigTemplate_8639f2184f064151f87399f5c0c878952ea54aa0e34be7ca69567b08ab1cbb1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95db222ab949b4ba799ecf8fc3808bafe6264eedb731481a6d0c391edb0c494d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_95db222ab949b4ba799ecf8fc3808bafe6264eedb731481a6d0c391edb0c494d->enter($__internal_95db222ab949b4ba799ecf8fc3808bafe6264eedb731481a6d0c391edb0c494d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_95db222ab949b4ba799ecf8fc3808bafe6264eedb731481a6d0c391edb0c494d->leave($__internal_95db222ab949b4ba799ecf8fc3808bafe6264eedb731481a6d0c391edb0c494d_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_8fffa4c35d81bf7f0b91edcafd0f8f10f4e439efdfd4092591ac6e40c9e26a1a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8fffa4c35d81bf7f0b91edcafd0f8f10f4e439efdfd4092591ac6e40c9e26a1a->enter($__internal_8fffa4c35d81bf7f0b91edcafd0f8f10f4e439efdfd4092591ac6e40c9e26a1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_8fffa4c35d81bf7f0b91edcafd0f8f10f4e439efdfd4092591ac6e40c9e26a1a->leave($__internal_8fffa4c35d81bf7f0b91edcafd0f8f10f4e439efdfd4092591ac6e40c9e26a1a_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_c795ccf3e0bf1dd58695dc12130711881ccdfdf8c13642799dd3ca2dd210d002 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c795ccf3e0bf1dd58695dc12130711881ccdfdf8c13642799dd3ca2dd210d002->enter($__internal_c795ccf3e0bf1dd58695dc12130711881ccdfdf8c13642799dd3ca2dd210d002_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_c795ccf3e0bf1dd58695dc12130711881ccdfdf8c13642799dd3ca2dd210d002->leave($__internal_c795ccf3e0bf1dd58695dc12130711881ccdfdf8c13642799dd3ca2dd210d002_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_03b9153571430cb8ccd81959702699ed984c0cfd3f0366b72f80dae8d9bc7be7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_03b9153571430cb8ccd81959702699ed984c0cfd3f0366b72f80dae8d9bc7be7->enter($__internal_03b9153571430cb8ccd81959702699ed984c0cfd3f0366b72f80dae8d9bc7be7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_03b9153571430cb8ccd81959702699ed984c0cfd3f0366b72f80dae8d9bc7be7->leave($__internal_03b9153571430cb8ccd81959702699ed984c0cfd3f0366b72f80dae8d9bc7be7_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_5b4e3316d93b28fd185c82839df56afe303a56b90b835142a3f317ecee8077ea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b4e3316d93b28fd185c82839df56afe303a56b90b835142a3f317ecee8077ea->enter($__internal_5b4e3316d93b28fd185c82839df56afe303a56b90b835142a3f317ecee8077ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_5b4e3316d93b28fd185c82839df56afe303a56b90b835142a3f317ecee8077ea->leave($__internal_5b4e3316d93b28fd185c82839df56afe303a56b90b835142a3f317ecee8077ea_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::base.html.twig", "/var/www/symtest/app/Resources/views/base.html.twig");
    }
}
