<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_915efd8d0f70f9f8f8d8db7f6a1dcf9cb851664723abdf8684f9ae1206876615 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e2b285ad6b538edc6deeb5326ddb628059364a93ea604406e084110b9d67c3c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e2b285ad6b538edc6deeb5326ddb628059364a93ea604406e084110b9d67c3c->enter($__internal_7e2b285ad6b538edc6deeb5326ddb628059364a93ea604406e084110b9d67c3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_7e2b285ad6b538edc6deeb5326ddb628059364a93ea604406e084110b9d67c3c->leave($__internal_7e2b285ad6b538edc6deeb5326ddb628059364a93ea604406e084110b9d67c3c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/repeated_row.html.php");
    }
}
