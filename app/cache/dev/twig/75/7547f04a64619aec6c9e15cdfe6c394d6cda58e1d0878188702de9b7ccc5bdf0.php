<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_a4223566c909cf5675668c2cacd5cdc59e29068ee6604f52df992009a1d8f9c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04c5fce48d6b619fe3c30e2bae7bb45f07ba6a8f54711b2e610681d5cdedd301 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04c5fce48d6b619fe3c30e2bae7bb45f07ba6a8f54711b2e610681d5cdedd301->enter($__internal_04c5fce48d6b619fe3c30e2bae7bb45f07ba6a8f54711b2e610681d5cdedd301_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_04c5fce48d6b619fe3c30e2bae7bb45f07ba6a8f54711b2e610681d5cdedd301->leave($__internal_04c5fce48d6b619fe3c30e2bae7bb45f07ba6a8f54711b2e610681d5cdedd301_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
