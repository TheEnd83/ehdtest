<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_3ca5485072681620d5f1b58ad9cb2762bbbfe2731a3208efbc67cddad5d3c79c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6a0671e90996b650bf5620bf6e849ac4daca4dfc2edefffc6d4aab800ef49863 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a0671e90996b650bf5620bf6e849ac4daca4dfc2edefffc6d4aab800ef49863->enter($__internal_6a0671e90996b650bf5620bf6e849ac4daca4dfc2edefffc6d4aab800ef49863_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6a0671e90996b650bf5620bf6e849ac4daca4dfc2edefffc6d4aab800ef49863->leave($__internal_6a0671e90996b650bf5620bf6e849ac4daca4dfc2edefffc6d4aab800ef49863_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_f579466c5a9b1e03066b437bfc83cf96530e052338cdc954ba222ff750478ad9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f579466c5a9b1e03066b437bfc83cf96530e052338cdc954ba222ff750478ad9->enter($__internal_f579466c5a9b1e03066b437bfc83cf96530e052338cdc954ba222ff750478ad9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_f579466c5a9b1e03066b437bfc83cf96530e052338cdc954ba222ff750478ad9->leave($__internal_f579466c5a9b1e03066b437bfc83cf96530e052338cdc954ba222ff750478ad9_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_7aa8272896f1526b41673a5008ed23e81b8c1f5aa57911d1542dce2b8ced9f86 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7aa8272896f1526b41673a5008ed23e81b8c1f5aa57911d1542dce2b8ced9f86->enter($__internal_7aa8272896f1526b41673a5008ed23e81b8c1f5aa57911d1542dce2b8ced9f86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_7aa8272896f1526b41673a5008ed23e81b8c1f5aa57911d1542dce2b8ced9f86->leave($__internal_7aa8272896f1526b41673a5008ed23e81b8c1f5aa57911d1542dce2b8ced9f86_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_51eea2a4c616bd367b8dd99293fe6e1627f6a59bf709952eb11a9611652c1852 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_51eea2a4c616bd367b8dd99293fe6e1627f6a59bf709952eb11a9611652c1852->enter($__internal_51eea2a4c616bd367b8dd99293fe6e1627f6a59bf709952eb11a9611652c1852_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_51eea2a4c616bd367b8dd99293fe6e1627f6a59bf709952eb11a9611652c1852->leave($__internal_51eea2a4c616bd367b8dd99293fe6e1627f6a59bf709952eb11a9611652c1852_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "/var/www/symtest/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
