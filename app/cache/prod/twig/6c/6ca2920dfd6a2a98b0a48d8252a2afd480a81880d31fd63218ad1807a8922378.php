<?php

/* AppBundle:partials:footer.html.twig */
class __TwigTemplate_90cf42f6e9ab632bc0ea56adce34b878e15edcccef0011b58c7a56d01ddccabe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"wrapper\">
    <div class=\"pure-g\">
        <div class=\"pure-u-1 pure-u-md-1-2\">
            <div class=\"footer-menu\">
                <ul>
                    <li><a href=\"#\">Home</a></li>
                    <li><a href=\"#\">Services</a></li>
                    <li><a href=\"#\">Portfolio</a></li>
                    <li><a href=\"#\">Contact</a></li>
                </ul>
            </div>
        </div>
        <div class=\"pure-u-1 pure-u-md-1-2\">
            <div class=\"copyrights\">
                <p>";
        // line 15
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " &copy;</p>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "AppBundle:partials:footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 15,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:partials:footer.html.twig", "/var/www/symtest/src/AppBundle/Resources/views/partials/footer.html.twig");
    }
}
