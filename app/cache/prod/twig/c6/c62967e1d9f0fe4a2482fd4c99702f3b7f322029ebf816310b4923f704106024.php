<?php

/* AppBundle:partials:header.html.twig */
class __TwigTemplate_17f9d5008e10d108301eba3bb63fe84557b374975a206e9c3ae4ff7da39122a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"wrapper\">
    <div class=\"pure-g\">
        <div class=\"logo-container pure-u-6-24\">
            <div class=\"logo\">
                BLU<span>E</span>ASY
            </div>
        </div>
        <div id=\"burger-menu\" class=\"burger-menu pure-u-18-24\">
            <img src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/burger.png"), "html", null, true);
        echo "\">
        </div>
        <div id=\"pri_nav\" class=\"menu-container pure-u-1 pure-u-md-18-24\">
            ";
        // line 12
        if ((isset($context["prim_nav"]) ? $context["prim_nav"] : null)) {
            // line 13
            echo "                <ul class=\"menu\">
                    ";
            // line 14
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["prim_nav"]) ? $context["prim_nav"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["menu_item"]) {
                // line 15
                echo "                    <li><a class=\"";
                echo (($this->getAttribute($context["menu_item"], "current", array())) ? ("current") : (""));
                echo "\" href=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["menu_item"], "url", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["menu_item"], "title", array()), "html", null, true);
                echo "</a></li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu_item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "                </ul>
            ";
        }
        // line 19
        echo "        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "AppBundle:partials:header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 19,  57 => 17,  44 => 15,  40 => 14,  37 => 13,  35 => 12,  29 => 9,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:partials:header.html.twig", "/var/www/symtest/src/AppBundle/Resources/views/partials/header.html.twig");
    }
}
