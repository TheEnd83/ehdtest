<?php

/* AppBundle:Homepage:homepage.html.twig */
class __TwigTemplate_71929abd3cc957d6e7cf3a315b048c6cf0ccfcb7439096d26dd13545c7ba4008 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("AppBundle::base.html.twig", "AppBundle:Homepage:homepage.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "AppBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        // line 6
        echo "    ";
        // line 7
        echo "    <div style=\"background-image: url('";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/banner.jpg"), "html", null, true);
        echo "')\" class=\"top-banner\">
        <div class=\"teaser\">
            <p>\"I am looking for unexpected.</p>
            <p> I'm looking for things I've never seen before.\"</p>
        </div>
    </div>
    ";
        // line 14
        echo "
    ";
        // line 16
        echo "    <div class=\"services\">
        <div class=\"wrapper\">
            <div class=\"pure-g\">
                <div class=\"section-title pure-u-1\">
                    <span class=\"underline\">Ser</span>vices
                </div>
                <div class=\"service-items\">
                    <div class=\"service-item pure-u-1 pure-u-md-1-4\">
                        <div class=\"service-item-container\">
                            <div class=\"image\"><img src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/blueimg1.png"), "html", null, true);
        echo "\"></div>
                            <div class=\"title\">";
        // line 26
        echo twig_escape_filter($this->env, (((isset($context["mozcast_temperature"]) ? $context["mozcast_temperature"] : null)) ? ((isset($context["mozcast_temperature"]) ? $context["mozcast_temperature"] : null)) : ("Coffee")), "html", null, true);
        echo "</div>
                            <div class=\"content\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        </div>
                    </div>
                    <div class=\"service-item pure-u-1 pure-u-md-1-4\">
                        <div class=\"service-item-container\">
                            <div class=\"image\"><img src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/blueimg2.png"), "html", null, true);
        echo "\"></div>
                            <div class=\"title\">";
        // line 33
        echo twig_escape_filter($this->env, (((isset($context["london_weather"]) ? $context["london_weather"] : null)) ? ((isset($context["london_weather"]) ? $context["london_weather"] : null)) : ("Istant")), "html", null, true);
        echo "</div>
                            <div class=\"content\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        </div>
                    </div>
                    <div class=\"service-item pure-u-1 pure-u-md-1-4\">
                        <div class=\"service-item-container\">
                            <div class=\"image\"><img src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/blueimg3.png"), "html", null, true);
        echo "\"></div>
                            <div class=\"title\">Serious</div>
                            <div class=\"content\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        </div>
                    </div>
                    <div class=\"service-item pure-u-1 pure-u-md-1-4\">
                        <div class=\"service-item-container\">
                            <div class=\"image\"><img src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/blueimg4.png"), "html", null, true);
        echo "\"></div>
                            <div class=\"title\">Frame</div>
                            <div class=\"content\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    ";
        // line 56
        echo "
    ";
        // line 58
        echo "    <div class=\"portfolio\">
        <div class=\"wrapper\">
            <div class=\"pure-g\">
                <div class=\"section-title pure-u-1\">
                    <span class=\"underline\">Por</span>tfolio
                </div>
                <div class=\"portfolio-items\">
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/portfolio1.jpg"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/portfolio2.jpg"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/portfolio3.jpg"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/portfolio1.jpg"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/portfolio2.jpg"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                    <div class=\"portfolio-item pure-u-1 pure-u-md-1-3\">
                        <div class=\"portfolio-item-container\">
                            <img src=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/portfolio3.jpg"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    ";
        // line 100
        echo "
    <div class=\"divide\">&nbsp;</div>

    ";
        // line 104
        echo "    <div style=\"background-image: url('";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/form-back.jpg"), "html", null, true);
        echo "')\" class=\"contact-form\">
        <div class=\"wrapper\">
            <div class=\"section-title pure-u-1\">
                <span class=\"underline\">Con</span>tact
            </div>
            <div class=\"form-container\">
                ";
        // line 110
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["contact_form"]) ? $context["contact_form"] : null), 'form_start');
        echo "
                    ";
        // line 111
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["contact_form"]) ? $context["contact_form"] : null), 'errors');
        echo "
                    <div class=\"form-row\">
                        <div class=\"pure-g\">
                            <div class=\"pure-u-1 pure-u-md-1-2 top-input\">
                                <div>
                                    ";
        // line 116
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : null), "name", array()), 'label');
        echo "<br />
                                    ";
        // line 117
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : null), "name", array()), 'widget');
        echo "
                                </div>
                                <div>
                                    ";
        // line 120
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : null), "email", array()), 'label');
        echo "<br />
                                    ";
        // line 121
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : null), "email", array()), 'widget');
        echo "
                                </div>
                            </div>
                            <div class=\"pure-u-1 pure-u-md-1-2 form-text\">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                        </div>
                    </div>
                    <div class=\"form-row\">
                        <div class=\"pure-g\">
                            <div class=\"pure-u-1\">
                                ";
        // line 133
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : null), "message", array()), 'label');
        echo "<br />
                                ";
        // line 134
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : null), "message", array()), 'widget', array("attr" => array("rows" => "7")));
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"form-row hidden\">
                        <div class=\"pure-g\">
                            <div class=\"pure-u-1\">
                                ";
        // line 141
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : null), "submitted", array()), 'label');
        echo "<br />
                                ";
        // line 142
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["contact_form"]) ? $context["contact_form"] : null), "submitted", array()), 'widget');
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"form-row\">
                        <div class=\"pure-g\">
                            <div class=\"pure-u-1\">
                                <input type=\"submit\" value=\"Message\">
                            </div>
                        </div>
                    </div>
                ";
        // line 153
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["contact_form"]) ? $context["contact_form"] : null), 'form_end');
        echo "
            </div>
        </div>
    </div>
    ";
        // line 158
        echo "

";
    }

    public function getTemplateName()
    {
        return "AppBundle:Homepage:homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  271 => 158,  264 => 153,  250 => 142,  246 => 141,  236 => 134,  232 => 133,  217 => 121,  213 => 120,  207 => 117,  203 => 116,  195 => 111,  191 => 110,  181 => 104,  176 => 100,  166 => 92,  158 => 87,  150 => 82,  142 => 77,  134 => 72,  126 => 67,  115 => 58,  112 => 56,  100 => 46,  90 => 39,  81 => 33,  77 => 32,  68 => 26,  64 => 25,  53 => 16,  50 => 14,  40 => 7,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle:Homepage:homepage.html.twig", "/var/www/symtest/src/AppBundle/Resources/views/Homepage/homepage.html.twig");
    }
}
